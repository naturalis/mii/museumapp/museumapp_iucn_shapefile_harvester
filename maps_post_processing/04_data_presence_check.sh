#!/bin/bash

INDIR=$1

if [ -z "$INDIR" ]; then
    echo "need an input directory"
    exit
fi


THRESHOLD=0.0001

if [[ ! -z $3 ]]; then
    THRESHOLD=$3
fi

GREEN=#63

echo "ratio threshold: $THRESHOLD"
echo "green: $GREEN*"

for FILE in $INDIR*.jpg; do

    OUT=$(convert "$FILE" -depth 8 -format %c histogram:info: | grep "$GREEN")

    SAVEIFS=$IFS
    IFS=$'\n'

    GREEN_PIXEL_COUNT=0

    for LINE in $OUT; do
        TMP=$(echo $LINE | awk '{print $1}' | sed 's/://')
        GREEN_PIXEL_COUNT=$(expr $GREEN_PIXEL_COUNT + $TMP)
    done

    IFS=$SAVEIFS

    if [[ $GREEN_PIXEL_COUNT == 0 ]]; then
        echo "skipping $FILE (found no green)"
        continue
    fi

    W_TIMES_H=$(convert "$FILE" -format "%wx%h" info:)
    W_TIMES_H=$(echo $W_TIMES_H | sed 's/x/*/')
    FULL_IMAGE=$(echo "($W_TIMES_H)" | bc)

    # echo $GREEN_PIXEL_COUNT
    # echo $FULL_IMAGE

    RATIO=$(echo "scale=10; ($GREEN_PIXEL_COUNT/$FULL_IMAGE)" | bc)
    HAS_GREEN=$(echo "scale=10; ($GREEN_PIXEL_COUNT/$FULL_IMAGE)>=$THRESHOLD" | bc)

    if [[ ! $HAS_GREEN == 1 ]]; then
        echo "possibly has no data: $FILE ($RATIO)"
    fi

done
